#include <iostream>
#include <math.h>
#include <map>

using namespace std;

map<long, int> mp;

int main() {
	ios_base::sync_with_stdio(0);
    int N, K;
	long input;
	cin >> N >> K;
	long a[N+ 1];
	for (int i = 0; i<N; i++){
		cin >> a[i];
	}
	for (int i = 0; i<K; i++){
		cin >> input;
		auto it = lower_bound(a, a+N, input);
		if (*it == input) cout << "YES";
		 else cout << "NO";
		cout << endl;
	}
}